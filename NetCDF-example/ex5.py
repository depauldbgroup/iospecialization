import netCDF4
import numpy as np

f = netCDF4.Dataset('/Users/tanu/Dropbox/Work/ResearchProjects/Containers/IOSpecialization/NetCDF-example/test-data-netCDF/rtofs_glo_3dz_f006_6hrly_reg3.nc')

# print variables
print(f.variables.keys())

# access an individual variable
temp = f.variables['temperature'] # temperature variable
print(temp)

# check dimension sizes
for d in f.dimensions.items():
print(d)

# inspecting variables
mt = f.variables['MT']
depth = f.variables['Depth']
x,y = f.variables['X'], f.variables['Y']
print(mt)
print(x)
print(y)

#Reading data from MT
time = mt[:]
print(time)

# from depth
dpth = depth[:]
print(depth.shape)
print(depth.dimensions)
print(dpth)

# apply conditionals
xx,yy = x[:],y[:]
print('shape of temp variable: %s' % repr(temp.shape))
tempslice = temp[0, dpth > 400, yy > yy.max()/2, xx > xx.max()/2]
print('shape of temp slice: %s' % repr(tempslice.shape))

# What is the sea surface temperature and salinity at 50N and 140W?

lat, lon = f.variables['Latitude'], f.variables['Longitude']
print(lat)
print(lon)
print(lat[:])

# extract lat/lon values (in degrees) to numpy arrays
latvals = lat[:]; lonvals = lon[:]

# a function to find the index of the point closest pt
# (in squared distance) to give lat/lon value.
def getclosest_ij(lats,lons,latpt,lonpt):
 # find squared distance of every point on grid
 dist_sq = (lats-latpt)**2 + (lons-lonpt)**2
 # 1D index of minimum dist_sq element
 minindex_flattened = dist_sq.argmin()
 # Get 2D index for latvals and lonvals arrays from 1D index
 return np.unravel_index(minindex_flattened, lats.shape)

iy_min, ix_min = getclosest_ij(latvals, lonvals, 50., -140)
print(iy_min)
print(ix_min)

sal = f.variables['salinity']
# Read values out of the netCDF file for temperature and salinity
print('%7.4f %s' % (temp[0,0,iy_min,ix_min], temp.units))
print('%7.4f %s' % (sal[0,0,iy_min,ix_min], sal.units))


