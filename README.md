# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Usecases for IOSpecialization of geoscience NetCDF library
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

NetCDF library: https://github.com/Unidata/netcdf-c

Instructions to classic build of NetCDF: https://www.unidata.ucar.edu/software/netcdf/docs/getting_and_building_netcdf.html#build_classic

Usecase:  Python program and dataset. This repo.

Video: https://www.youtube.com/watch?v=w0cMCKGp9RQ

One aspect that one should consider as we build is parallel I/O which is quite common for scientific data and NetCDF: https://trac.mcs.anl.gov/projects/parallel-netcdf 



### Who do I talk to? ###

* tanu@cdm.depaul.edu
